/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libs;

import java.security.MessageDigest;


/**
 * @author Ricardo
 */
public class Crypto {
    //ATRIBUTOS
    private String texto;
    private String sal;
    private String algoritmo;

    //CONSTRUTORES
    public Crypto() {

    }

    public Crypto(String texto, String sal, String algoritmo) {
        this.setTexto(texto);
        this.setSal(sal);
        this.setAlgoritmo(algoritmo);
    }

    //GET SET
    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getSal() {
        return sal;
    }

    public void setSal(String sal) {
        this.sal = sal;
    }

    public String getAlgoritmo() {
        return algoritmo;
    }

    public void setAlgoritmo(String algoritmo) {
        this.algoritmo = algoritmo;
    }

    //METODOS
    public String cifrar() {
        //DECLARAR VARIRIAVEL PARA GUARDAR CIFRA
        String cifra = null;

        try {
            //DECLARAR MESSAGE DIGEST COM A STRING DO ALGORITMO
            MessageDigest md = MessageDigest.getInstance(this.getAlgoritmo());

            //OBTER BYTES DO SAL
            md.update(this.getSal().getBytes("UTF-8"));

            //DECLARAR VAR DE ARRAY DE BYTES DA STRING PARA CONVERTER
            byte[] bytes = md.digest(this.getTexto().getBytes("UTF-8"));

            //DECLARAR STRING BUILDER
            StringBuilder sb = new StringBuilder();

            //PERCORRER ARRAY E CONSTRUIR HASH
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }

            //PASSAR HASH PARA STRING DE RETORNO
            cifra = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //RETORNAR HASH
        return cifra;
    }


}
