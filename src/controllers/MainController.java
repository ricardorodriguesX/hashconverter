/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import libs.Crypto;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

/**
 * FXML Controller class
 *
 * @author Ricardo
 */
public class MainController implements Initializable {

    /**
     * Initializes the controller class.
     */
    //VAR FMXL
    @FXML
    private RadioButton md2;
    @FXML
    private RadioButton md5;
    @FXML
    private RadioButton sha1;
    @FXML
    private RadioButton sha224;
    @FXML
    private RadioButton sha256;
    @FXML
    private RadioButton sha384;
    @FXML
    private RadioButton sha512;
    @FXML
    private TextField texto;
    @FXML
    private TextField sal;
    @FXML
    private TextArea cifra;

    //MINHAS VAR
    private ToggleGroup toggleGroup = new ToggleGroup();
    private Preferences preferencias;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        //SETAR GRUPO
        md5.setToggleGroup(toggleGroup);
        md2.setToggleGroup(toggleGroup);
        sha1.setToggleGroup(toggleGroup);
        sha256.setToggleGroup(toggleGroup);
        sha384.setToggleGroup(toggleGroup);
        sha512.setToggleGroup(toggleGroup);
        sha224.setToggleGroup(toggleGroup);

        //DEFINIR NODE DAS PREFENCIAS
        preferencias = Preferences.userRoot().node(this.getClass().getName());

        //PUT DO TEXTO DEFAULT
        preferencias.put("algoritmo", "MD5");
    }

    //METODOS FXML
    @FXML
    protected void cifrar() {
        //INSTANCIRAR CRYPTO
        Crypto crypto = new Crypto();

        crypto.setTexto(texto.getText());
        crypto.setSal(sal.getText());
        crypto.setAlgoritmo(preferencias.get("algoritmo", "MD5"));

        cifra.setText(crypto.cifrar());

    }

    @FXML
    protected void setPreferencias(ActionEvent evento) {
        //CAST PARA RADIO BUTTON
        RadioButton radioButton = (RadioButton) evento.getSource();

        //DEFINIR NODE DAS PREFENCIAS
        preferencias = Preferences.userRoot().node(this.getClass().getName());

        //PUT DO TEXTO DO RADIO BUTTON SELECIONADO
        preferencias.put("algoritmo", radioButton.getText());
    }

}
