package bin;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @author Ricardo
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        //CRIAR PARENTE
        Parent root = FXMLLoader.load(getClass().getResource("/views/main.fxml"));

        //SET DO TITULO DA JANELA
        primaryStage.setTitle("HashConverter");

        //SET CENA
        primaryStage.setScene(new Scene(root));

        //PROIBIR RESIZE DA JANELA
        primaryStage.setResizable(false);

        //MOSTRAR CENA
        primaryStage.show();
    }

}
